class ApplicantsController < ApplicationController
  def index
    @jobs = Job.all
    @applicants = Applicant.where(job_id: @jobs)
    @uniq_skill_count = Skill.where(applicant_id: @applicants).map(&:name).uniq.count
  end
end
