class Job < ApplicationRecord
  has_many :applicants

  def uniq_applicant_skill_count
    Skill.where(applicant_id: self.applicants).count
  end
end
