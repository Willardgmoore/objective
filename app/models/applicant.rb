class Applicant < ApplicationRecord
  has_one :job
  has_many :skills


  def skills_count
    self.skills.count
  end

end
