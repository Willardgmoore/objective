namespace :import => :environment do
  data = JSON.parse(File.read('db/data.json'))

  data['jobs'].each do |job|
    Job.create(job.to_h)
  end

  data['applicants'].each do |applicant|
    Applicant.create(applicant.to_h)
  end

  data['skills'].each do |skill|
    Skill.create(skill.to_h)
  end
end
